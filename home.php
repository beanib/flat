<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php   wp_enqueue_script( 'gmpas', get_template_directory_uri() . '/assets/js/gmaps.min.js', array( ), false, 'all' ); ?>

  <script type="text/javascript">
    var map;
    $(document).ready(function(){
      map = new GMaps({
        el: '#map',
        lat: -21.546187,
        lng: -64.722909,
      });
      map.addMarker({  
        lat: -21.544737,
        lng: -64.722422,
        title: 'DTIC',
        infoWindow: {
          content: '<p>Departamento de Tecnologias de Informacion y Comunicación</p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/dtic-edificio.jpg" alt="" width="100%" height="100%" class="responsive">'
        }
      });
    });
  </script>

<?php get_header(); ?>

  <div class="page-title">
  <h1>Mensajes</h1>
  <?php echo do_shortcode('[recent-posts]'); ?>
  </div>

  <div class="page-title">
  <h1>Nuestra ubicación</h1>
    <div id="map" class="thumbnail"></div>
  </div>

  <div class="page-title">
    <h1>Horarios<small> de atención</small></h1>
    <table class="table">
      <thead>
        <tr>
          <td>Dias</td>
          <td>Manaña</td>
          <td>Tarde</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Lunes a viernes</td>
          <td>8:00 - 12:00</td>
          <td>13:00 - 19:00</td>
        </tr>
      </tbody>
    </table>
  </div>

<?php get_footer(); ?>

