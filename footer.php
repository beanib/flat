<?php
    $tel_contact = of_get_option('tel_contact','');
    $email_contact = of_get_option('email_contact','');
    $dir_contact = of_get_option('dir_contact','');
    $cp_contact = of_get_option('cp_contact','');
?>
				<?php if ( apply_filters( 'show_flat_credits', true ) ) { ?>
					<div class="site-info">
						<?php
							if(($tel_contact)&&($email_contact)&&($dir_contact)&&($cp_contact)) {
					      		echo '<span><strong>Teléfono: </strong>' . $tel_contact . ' &#8226; ';
					      		echo '<strong>Web: </strong>' . $email_contact . ' &#8226; ';
					      		echo '<strong>Dirección: </strong>' . $dir_contact . ' &#8226; ';
					      		echo '<strong></strong>' . $cp_contact . '</span><br>';
								echo '<strong>Copyright &#169; Universidad Autonoma Juan Misael Saracho - 2014 </strong><br><br>';
								?>
							    <img src="<?=bloginfo('stylesheet_directory')?>/assets/img/UAJMS.png" />
							    <?php
					      	}
					   	?>
					</div><!-- .site-info -->
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>