<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	$options = array();

	/********************************************************************************
	/* Pestaña Configuración general
	********************************************************************************/

	$options[] = array(
		'name' => __('Página de inicio', 'options_framework_theme'),
		'type' => 'heading' );

		$list_cat = array(
			        '1' => 'Naranja - Moderado',
			        '2' => 'Roja - Alto',
			        '3' => 'Morada - Muy Alto',
			       	'4' => 'Violeta - Estrema',
    			);

		$options[] = array(
			'name' => __('Input Select Wide', 'options_framework_theme'),
		 	'desc' => __('A wider select box.', 'options_framework_theme'),
		 	'id' => 'example_select_wide',
		 	'std' => '1',
		 	'type' => 'select',
		 	//'class' => 'mini', //mini, tiny, small
		 	'options' => $list_cat);

	/********************************************************************************
	/* INFORMACIÓN DE CONTACTO
	********************************************************************************/
	$options[] = array(
		'name' => __('Información de contacto', 'options_framework_theme'),
		'type' => 'heading');

	//Teléfono de contacto
	$options[] = array(
		'name' => __('Teléfono de contacto', 'options_framework_theme'),
		'desc' => __('Instroduzca el número de teléfono de contacto.', 'options_framework_theme'),
		'id' => 'tel_contact',
		'std' => '4 6650825 . Tel. Fax: 4 6646917',
		'class' => 'mini',
		'type' => 'text'
	);

	//Email de contacto
	$options[] = array(
		'name' => __('E-mail de contacto', 'options_framework_theme'),
		'desc' => __('Instroduzca el Email de contacto, a este correo llegarán los email enviados desde el formulario de contacto.', 'options_framework_theme'),
		'id' => 'email_contact',
		'std' => 'dtic.uajms.edu.bo',
		'class' => 'mini',
		'type' => 'text'
	);

	//Dirección de contacto
	$options[] = array(
		'name' => __('Dirección de contacto', 'options_framework_theme'),
		'desc' => __('Instroduzca su dirección actual.', 'options_framework_theme'),
		'id' => 'dir_contact',
		'std' => 'Campus Universitario - Zona El Tejar',
		'class' => 'Default Value',
		'type' => 'text'
	);

	//Código postal
	$options[] = array(
		'name' => __('Código postal', 'options_framework_theme'),
		'desc' => __('Instroduzca su código postal.', 'options_framework_theme'),
		'id' => 'cp_contact',
		'std' => 'Tarija - Bolivia',
		'class' => 'mini',
		'type' => 'text'
	);


	return $options;
}?>