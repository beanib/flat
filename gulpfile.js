var gulp = require('gulp');                     // Gulp!

var less = require('gulp-less');                // Less
var prefix = require('gulp-autoprefixer');      // Autoprefixr
var minifycss = require('gulp-minify-css');     // Minify CSS
var concat = require('gulp-concat');            // Concat files
var uglify = require('gulp-uglify');          // Uglify javascript
var rename = require('gulp-rename');            // Rename files
var util = require('gulp-util');                // Writing stuff




//
//    Compile all CSS for the site
//
//////////////////////////////////////////////////////////////////////


  gulp.task('less', function (){
    gulp.src([
      'assets/less/main.less',         // Gets normalize
      'assets/css/editor-style.less'])                                   // Gets the apps scss
      .pipe(less({style: 'compressed', errLogToConsole: true}))
      .pipe(concat('main.css'))                                  // Concat all css
      .pipe(rename({suffix: '.min'}))                            // Rename it
      //.pipe(minifycss())                                         // Minify the CSS
      .pipe(gulp.dest('assets/css/'))                            // Set the destination to assets/css
      util.log(util.colors.yellow('Sass compilado & minificado'));  // Output to terminal
  });


//
//    Get all the JS, concat and uglify
//
//////////////////////////////////////////////////////////////////////


  gulp.task('javascripts', function(){
    gulp.src([
      'assets/vendor/jquery/dist/jquery.min.js',     // Gets Jquery
      //'assets/vendor/gmaps/gmaps.js',     // Gets Jquery
      // Gets Bootstrap JS change to only include the scripts you'll need

      'assets/vendor/bootstrap/js/transition.js',
      'assets/vendor/bootstrap/js/alert.js',
      'assets/vendor/bootstrap/js/button.js',
      'assets/vendor/bootstrap/js/carousel.js',
      'assets/vendor/bootstrap/js/collapse.js',
      'assets/vendor/bootstrap/js/dropdown.js',
      'assets/vendor/bootstrap/js/modal.js',
      'assets/vendor/bootstrap/js/tooltip.js',
      'assets/vendor/bootstrap/js/popover.js',
      'assets/vendor/bootstrap/js/scrollspy.js',
      'assets/vendor/bootstrap/js/tab.js',
      'assets/vendor/bootstrap/js/affix.js',
      //'assets/js/plugins/*.js',
      'assets/js/_*.js'])                   // Gets all the user JS _*.js from assets/js
      .pipe(concat('scripts.js'))           // Concat all the scripts
      .pipe(rename({suffix: '.min'}))       // Rename it
      //.pipe(uglify())                       // Uglify(minify)
      .pipe(gulp.dest('assets/js/'))        // Set destination to assets/js
      util.log(util.colors.yellow('Javascripts compilado & minificado'));    // Output to terminal
  });


//
//    Copy bower components to assets-folder
//
//////////////////////////////////////////////////////////////////////


  gulp.task('copy', function(){
    gulp.src('assets/vendor/modernizr/modernizr.js')     // Gets Modernizr.js
    .pipe(uglify())                       // Uglify(minify)
    .pipe(rename({suffix: '.min'}))               // Rename it
    .pipe(gulp.dest('assets/js/'));               // Set destination to assets/js
    util.log(util.colors.yellow('Modernizr copiado'));     // Output to terminal

    gulp.src('assets/vendor/respond/src/respond.js')     // Gets Respond.js
    .pipe(uglify())                       // Uglify(minify)
    .pipe(rename({suffix: '.min'}))               // Rename it
    .pipe(gulp.dest('assets/js/'));               // Set destination to assets/js
    util.log(util.colors.yellow('Respond copiado'));     // Output to terminal

    gulp.src('assets/vendor/gmaps/gmaps.js')     // Gets gmaps.js
    .pipe(uglify())                       // Uglify(minify)
    .pipe(rename({suffix: '.min'}))               // Rename it
    .pipe(gulp.dest('assets/js/'));               // Set destination to assets/js
    util.log(util.colors.yellow('Gmaps copiado'));     // Output to terminal
  });



gulp.task('default', ['less', 'javascripts', 'copy']);